from IsThirdDigitZero import isThirdDigitZero  # import the code-under-test
from pythonfuzz.main import PythonFuzz         # import fuzz test infrastructure
 
# The fuzz engine calls a function called `fuzz` in the fuzz target and
# passes it random data, so we need to define a function with that name, that
# accepts 1 parameter.

@PythonFuzz                        # Python decorator required by fuzz test infrastructure
def fuzz(random_data):             # accept random data...
    isThirdDigitZero(random_data)  # ...and pass it on to the code-under-test
    
if __name__ == '__main__':         # required by fuzz test infrastructure
    fuzz()
