def isThirdDigitZero(my_ints):
    """Return True if and only if the third value passed in is 0."""
    return my_ints[2] == 0  # Because you start counting from 0, 
                            # the "2" here refers to the 3rd digit passed in.
